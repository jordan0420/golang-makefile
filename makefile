# This is what we will name the binary
BINARY=project
# These are the values we want to pass for Version and Build
Version=1.0.0
Build=`date +%FT%T%z`

# Setup the -ldflags option for go build here, Interpolate the variable values
LDFLAGS=-ldflags "-X main.Version=$(Version) -X main.Build=$(Build)"

# Builds the project
build:
	go build $(LDFLAGS) -o $(BINARY)

clean:
	if [ -f $(BINARY) ] ; then rm $(BINARY); fi
.PHONY: clean install
